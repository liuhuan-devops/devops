`部分yml需要修改一些变量后才能使用`
### docker安装：
1、wget https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo -O /etc/yum.repos.d/docker-ce.repo

2、yum -y install docker-ce-19.03.15-3.el7 #也可以直接docker-ce使用最新版

3、systemctl enable docker && systemctl start docker

4、配置镜像加速，数据目录等配置。
```
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": [
    "https://docker.rainbond.cc",
    "https://mirror.iscas.ac.cn",
    "https://docker.mirrors.ustc.edu.cn",
    "https://hub-mirror.c.163.com",
    "https://hub.uuuadc.top",
    "https://docker.anyhub.us.kg",
    "https://dockerhub.jobcher.com",
    "https://dockerhub.icu",
    "https://docker.ckyl.me",
    "https://docker.awsl9527.cn",
    "https://mirror.baidubce.com"
  ],
  "data-root": "/home/docker",
  "log-driver":"json-file",
  "log-opts": {"max-size":"200m", "max-file":"5"},
  "insecure-registries":["192.168.23.130:30002","harbor.com:30002"],
  "exec-opts": ["native.cgroupdriver=systemd"]
}
EOF
```
5、system daemon-reload && systemctl restart docker

### docker-compose安装：
1、
```
curl -L https://g.nite07.org/docker/compose/releases/download/v2.24.7/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
```

2、chmod +x /usr/local/bin/docker-compose

