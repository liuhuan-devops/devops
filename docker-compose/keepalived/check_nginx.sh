#!/bin/bash
 
while true
do
    if [ -z "$(lsof -i :80)" ]; then
        docker start nginx
    fi
 
    sleep 5
 
    if [ -z "$(lsof -i :80)" ]; then
        docker stop keepalived-master
    fi
 
    sleep 5
 
    if [ -z "$(lsof -i :80)" ]; then
        docker start nginx
    fi
 
    sleep 5
 
    if [ -z "$(lsof -i :80)" ]; then
        docker stop keepalived-master
    fi

done