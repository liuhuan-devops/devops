### gitlab-runner安装

1、curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bash

2、yum list gitlab-runner --showduplicates | sort -r     #版本排序，指定安装版本

3、yum -y install gitlab-runner

### gitlab-runner修改为root用户

1.通过ps aux|grep gitlab-runner命令查看gitlab-runner进程，可以查看到gitlab-runner的工作目录和默认用户等一系列相关信息。

2.通过该命令sudo gitlab-runner uninstall可以卸载掉gitlab-runner默认用户

3.重新安装gitlab-runner并将用户设置为rootgitlab-runner install --working-directory /home/gitlab-runner --user root

4.sudo service gitlab-runner restart重启gitlab-runner

5.再通过第一步的命令查看gitlab-runner看默认用户是否变成root

### 建议升级git版本
