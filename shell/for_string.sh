#!/bin/bash
for((i=1;i<10000;i++))
do
        echo "      server {" >> test
        echo "         listen 4`printf %04d $i`;" >> test
        echo "         proxy_pass 10.0.0.5:`printf %04d $i`;" >> test
        echo "      }" >> test
done