开机自启动的方式，
1、通过rc.local文件的特性启动
cat /etc/rc.local
bash /home/shell/restart.sh
2、通过crontab
crontab -e
@reboot /home/shell/restart.sh
3、通过systemd实现
vim /lib/systemd/system/restart.service

[Unit]
Description=restart
After=default.target

[Service]
ExecStart=/root/script/restart.sh

[Install]
WantedBy=default.target

systemctl daemon-reload
systemctl restart restart.service