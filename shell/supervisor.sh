#!/bin/bash
read -p "请问是否安装了supervisor?(y/n)" choose
 if [ $choose == n ];then
yum -y install python-setuptools
easy_install supervisor
# yum install -y python-pip && pip install pip --upgrade RUN pip install --no-deps --ignore-installed --pre supervisor 
echo_supervisord_conf > /etc/supervisord.conf
echo "[include]" >> /etc/supervisord.conf
echo "files = /usr/local/share/supervisor/*.conf" >> /etc/supervisord.conf
cat >/usr/lib/systemd/system/supervisord.service<< EOF
[Unit]
Description=Supervisor daemon
[Service]
Type=forking
ExecStart=/usr/bin/supervisord -c /etc/supervisord.conf
ExecStop=/usr/bin/supervisorctl shutdown
ExecReload=/usr/bin/supervisorctl reload
KillMode=process
Restart=on-failure
RestartSec=42s

[Install]
WantedBy=multi-user.target
EOF
sed -i 's/tmp/opt/' /etc/supervisord.conf
systemctl enable supervisord.service
systemctl restart supervisord.service
echo "---------------------------supervisor安装成功----------------------------------------------"
read -p "请输入你要添加为守护进程的项目:" project
read -p "请输入你的服务启动命令(绝对路径)：" command
mkdir -p /usr/local/share/supervisor/$project.log
cat >/usr/local/share/supervisor/$project.conf<<EOF
[program:$project]
user=root
directory=/usr/local/app
command=$command
autostart=true
autorestart=true
startsecs=3
stdout_logfile=/usr/local/share/supervisor/$project.log/out.log
EOF
supervisorctl reload
supervisorctl restart $project

 elif [ $choose == y ];then
read -p "请输入你要添加为守护进程的项目:" project
read -p "请输入你的服务启动命令(绝对路径)：" command
mkdir -p /usr/local/share/supervisor/$project.log
cat >/usr/local/share/supervisor/$project.conf<<EOF
[program:$project]
user=root
directory=/home/yzhj
command=$command
autostart=true
autorestart=true
startsecs=3
stdout_logfile=/usr/local/share/supervisor/$project.log/out.log
EOF
supervisorctl reload
supervisorctl restart $project
 else
	echo "输入有误"
fi
