# -*- coding:utf-8 -*-
# Author: li Shang

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import smtplib
from email.header import Header
import sys



title = sys.argv[1]
msg = ""
m_file = sys.argv[2]
f = open(m_file, 'r')
for line in f:
    msg += line

mail_host = "smtp.qiye.aliyun.com"
mail_user = "xxx"  # 发送邮箱
mail_pass = "xxx"

receiver = sys.argv[3]
receivers = []  # 接收邮件，可设置为你的QQ邮箱或者其他邮箱
receivers = [receiver]

# 创建一个带附件的实例
message = MIMEMultipart()
message['From'] = Header("xxx", 'utf-8')
message['To'] = ",".join(receivers)
subject = title
message['Subject'] = Header(subject, 'utf-8')

# 邮件正文内容
message.attach(MIMEText(msg, 'plain', 'utf-8'))

file = sys.argv[4]
# 构造附件1，传送当前目录下的 id_rsa 文件
att1 = MIMEText(open("/root/test/vpn/"+file, 'rb').read(), 'base64', 'utf-8')
att1["Content-Type"] = 'application/octet-stream'
# 这里的filename可以任意写，写什么名字，邮件中显示什么名字
att1["Content-Disposition"] = 'attachment; filename="openvpn"'
message.attach(att1)

try:
    server = smtplib.SMTP()
    server.connect(mail_host)
    server.login(mail_user, mail_pass)
    server.sendmail(mail_user, receivers, message.as_string())
    server.close()
    print("邮件发送成功")
except smtplib.SMTPException:
    print("Error: 无法发送邮件")
