import requests
import sys

username = sys.argv[1]
password = sys.argv[2]
name = sys.argv[3]

url = "http://xxx/api/users/v1/users/"

payload = "{\n    \"name\": \""+name+"\",\n    \"username\": \""+username+"\",\n    \"password\": \""+password+"\",\n    \"email\": \"test@ts-it.cn\",\n    \"groups\": [\n    \"061b1de9-6d26-457a-85d4-d617c8caf92e\"\n  ]\n}"

headers = {
    "Content-Type":"application/json",
    "Authorization":"Token xxx"
}

response=requests.request("POST",url, data=payload, headers=headers)

print(response.text)
print(payload)
