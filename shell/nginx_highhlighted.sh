#!/bin/bash
mkdir -p ~/.vim/syntax
echo "au BufRead,BufNewFile /usr/local/nginx/conf/* set ft=nginx" >> ~/.vim/filetype.vim
wget http://www.vim.org/scripts/download_script.php?src_id=19394 -O ~/.vim/syntax/nginx.vim
