#!/bin/bash
yum -y remove `yum list installed|grep java`
yum -y install java-1.8.0-openjdk*

wget https://archive.apache.org/dist/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz
tar xf apache-maven-3.6.3-bin.tar.gz -C /usr/local/app/maven
#cat <<EOF>> /etc/profile
#export MAVEN_HOME=/usr/local/app/maven/apache-maven-3.6.3
#export PATH=$PATH:$JAVA_HOME/bin:$MAVEN_HOME/bin
#EOF
echo "export M2_HOME=/usr/local/app/maven/apache-maven-3.6.3" >> /etc/profile
echo "export PATH=${M2_HOME}/bin:$PATH" >> /etc/profile
source /etc/profile
mvn -version
