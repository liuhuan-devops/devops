#!/bin/bash
nohup ./mysql-lock.sh >mysql-lock.log 2>&1 &
sleep 10
echo "开始备份"
startTime=`date +%Y%m%d-%H:%M:%S`
startTime_s=`date +%s`
####记得修改备份路径！！！
expect <<EOF
    set timeout -1
    spawn rsync -zav /data/mysql root@10.46.75.233:/data/mysql/db9-a/data-a 
    expect "*password:"
    send "*W|6Ow)|g-I5\n"
    expect eof
EOF

endTime=`date +%Y%m%d-%H:%M:%S`
endTime_s=`date +%s`
sumTime=`echo "scale=3; ($endTime_s-$startTime_s)/3600" |bc`
echo "拷贝完成"
echo "$startTime ---> $endTime" "Total:$sumTime 小时"

id=$(ps aux | grep "mysql-lock.sh" | grep -v grep | cut -c 9-15)
if [ $id ]
then
  kill -9 $id
  echo "mysql解锁成功"
else 
  echo "ERR: mysql 未加锁，请检查"
fi
###  传输备份结果修改 节点名称
expect <<EOF
    set timeout -1
    spawn ssh root@10.46.75.233 
    expect "*password:"
    send "*W|6Ow)|g-I5\n"
    expect "*#"
    send "date '+%Y-%m-%d %H:%M:%S A节点备份完成 ' >>/data/mysql/backup.log\n"
    expect "*#"
    send "logout\n"
    expect eof
EOF