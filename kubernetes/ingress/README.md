## 采用的nginx官方提供的ingress-nginx控制器：https://kubernetes.github.io/ingress-nginx/deploy/
这里需要注意以下几点：


```
deployment.spec.template.spec下添加hostNetwork: true，从而使其直接使用物理机网络。会使用节点的80，443端口

nodePort模式占用的是svc的nodePort端口。而hostNetwork则需要占用物理机的80和443端口。
```