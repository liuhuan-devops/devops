
```
# 服务端
yum -y install nfs-utils rpcbind
systemctl start nfs
systemctl start rpcbind
mkdir /nfs-data

vim /etc/exports
/nfs-data 192.168.16.0/24(rw,no_root_squash)

exportfs -arv
# 客户端
yum -y install nfs-utils rpcbind
mount 192.168.16.6:/nfs-data /nfs-data
测试能否挂载
```

使用动态存储时，pvc一直处于pending状态，/etc/kubernetes/mainsets/kube-apiserver.yml
添加- --feature-gates=RemoveSelfLink=false解决。
问题链接：https://github.com/kubernetes-sigs/nfs-subdir-external-provisioner/issues/25
该问题好像是1.20版本新添加的特性导致的，其他版本可以不用添加
