#### 介绍
# devops
| shell | 日常运维常用脚本 |
|---|---|
| docker-compose | docker-compose服务部署文件 |
| kubernetes | k8s服务部署文件 |
| gitlab-ci | gitlab-ci流水线配置文件 |
| jenkins | jenkinsfile流水线配置文件 |

            
#### 安装教程

1. docker-compose下的文件，提供通过yaml文件配置和一些指定的配置文件，实现服务的一键部署，让新手小白也可以直接通过docker-compose up -d秒部署所有服务。
